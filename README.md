This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# BUD Front-End Developer Exercise!

Candidate: Rafal Orzechowski

## Demo

Please take a look at [demo](https://bud-frontend-developer-exercise.netlify.app).

You can also run it locally. I've left some original README part from CRA below.

## Additional information

The card component is in reality just a modified version of [the original card](https://codepen.io/FilipVitas/pen/ddLVZx).

Author of this codepen: Filip Vitas.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
