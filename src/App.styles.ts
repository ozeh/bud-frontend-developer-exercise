import styled from 'styled-components';
import { colors } from './shared/colors';

const Wrapper = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  min-height: 100vh;
  width: 100%;
  max-width: 1200px;
  margin: auto;
  position: relative;
  background: ${colors.secondary};
`;

export default { Wrapper };
