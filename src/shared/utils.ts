import moment from 'moment';
import { BalanceTypes } from './types/account';

export const formatDate = (date: string) =>
  date && moment(date).format('DD/MM/YYYY');

const localizeByCurrency = (currency: string) => {
  switch (currency) {
    case 'EUR':
      return { countryCode: 'de-DE', currency: 'EUR' };
    default:
      return { countryCode: 'en-GB', currency: 'GBP' };
  }
};

export const formatCurrency = (amount: number, currencyISO: string) => {
  const { countryCode, currency } = localizeByCurrency(currencyISO);
  return new Intl.NumberFormat(countryCode, {
    style: 'currency',
    currency,
    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
  }).format(amount);
};

export const formatDateToTimestamp = (date: string) =>
  date && moment(date).format('x');
