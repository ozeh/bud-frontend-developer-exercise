export const colors = {
  text: '#111111',
  secondary: '#1444673',
  primary: '#14233C',
  danger: '#E64B5F',
  success: '#1E788C',
  warning: '#EB824B',
  white: '#FFF',
  black1: '#333',
  black2: '#222',
  grey: '#5a5a5a',
  lightGrey: '#f7f7f7 ',
};
