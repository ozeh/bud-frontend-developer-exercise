export interface AccountInfoTypes {
  id: string;
  provider: ProviderTypes;
  balance: BalanceTypes;
  transactions: TransactionTypes[];
}

export interface ProviderTypes {
  title: string;
  account_number: string;
  sort_code: string;
  description: string;
}
export interface BalanceTypes {
  amount: number;
  currency_iso: string;
}

export interface TransactionTypes {
  id: string;
  date: string;
  description: string;
  category_title: string;
  amount: {
    value: number;
    currency_iso: string;
  };
  balance?: number;
}
