import axios from 'axios';

const endpointUrl = 'https://www.mocky.io/v2/5c62e7c33000004a00019b05';

const options = {
  headers: {
    'Content-Type': 'application/json',
  },
};

const loadAccountInfo = () => axios.get(endpointUrl, options);

export default {
  loadAccountInfo,
};
