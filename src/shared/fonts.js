export const font = Object.freeze({
  primary: "'Montserrat', sans-serif",
  secondary: "'Roboto', sans-serif",
});
