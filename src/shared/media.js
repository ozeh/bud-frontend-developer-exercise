const size = {
  mobileSmall: '576px',
  mobileBig: '768px',
  tablet: '1024px',
};

export const device = {
  mobileSmall: `(max-width: ${size.mobileSmall})`,
  mobileBig: `(max-width: ${size.mobileBig})`,
  tablet: `(max-width: ${size.tablet})`,
};

export const onMobileElementWidth = '75%';
