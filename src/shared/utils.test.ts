import { formatDate, formatCurrency, formatDateToTimestamp } from './utils';

describe('utils.ts', () => {
  const div = document.createElement('div');

  describe('formatDate', () => {
    it('Formats date to DD/MM/YYYY', () => {
      const dateToFormat = 'Tue, 28 Jul 2020 19:54:32 EDT';
      const result = formatDate(dateToFormat);

      expect(result).toBe('29/07/2020');
    });
  });

  describe('formatCurrency', () => {
    it('Formats number amount to specific currency', () => {
      const amount = 12345;
      const result = formatCurrency(amount, 'EUR');

      expect(result).toBe('€12,345.00');
    });
  });

  describe('formatDateToTimestamp', () => {
    it('Formats date to timestamp', () => {
      const date = 'Tue, 28 Jul 2020 19:54:32 EDT';
      const result = formatDateToTimestamp(date);

      expect(result).toBe('1595980472000');
    });
  });
});
