import { createGlobalStyle } from 'styled-components';
import { colors } from './colors';
import { font } from './fonts';
import { device } from './media';

const globalStyle = createGlobalStyle`
    *,
    *:before,
    *:after {
        box-sizing: border-box;
    }

    html, body {
        height: 100%;
        margin: 0;
        padding: 0;
        width: 100%;
        overflow-x: hidden;
        -ms-overflow-style: -ms-autohiding-scrollbar;
        font-synthesis: none;
        -moz-font-feature-settings: 'kern';
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        direction: ltr;
    }

  body {
    color: ${colors.text};
    font-family: ${font.primary};
    font-weight: normal;
    font-size: 15px;
    line-height: 1.3;
    font-weight: 400;
  }

    #root {
        height: 100%;
        width: 100%;
        overflow-x: hidden;
    }

  a, button {
    user-select: none;
    outline: none;
    -webkit-tap-highlight-color: rgba(0,0,0,0);
    -webkit-tap-highlight-color: transparent;
  }

  a {
      cursor: pointer;
        color: inherit;
        text-decoration: none;
        transition: 0.2s color ease-in-out;
        color: ${colors.secondary};
    }

        ul {
            padding: 0;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: ${font.secondary};
            font-weight: 500;
            margin: 0;
        }

        h1 {
            font-size: 2.5rem;
        }

        h2 {
            font-size: 2rem;
        }

        h3 {
            font-size: 1.8rem;
        }

        h4 {
            font-size: 1.6rem;
        }

        h5 {
            font-size: 1.4rem;
        }

        h6 {
            font-size: 1rem;
        }

        @media ${device.tablet} {
          h1 {
                font-size: 2.2rem;
            }

            h2 {
                font-size: 1.8rem;
            }

            h3 {
                font-size: 1.6rem;
            }

            h4 {
                font-size: 1.4rem;
            }

            h5 {
                font-size: 1.2rem;
            }

            h6 {
                font-size: 1rem;
            }
            body {
              font-size: 1rem;
            }
      }

      @media ${device.mobileBig} {
          h1 {
                font-size: 2rem;
            }

            h2 {
                font-size: 1.6rem;
            }

            h3 {
                font-size: 1.4rem;
            }

            h4 {
                font-size: 1.2rem;
            }

            h5 {
                font-size: 1.1rem;
            }

            h6 {
                font-size: 1rem;
            }
            body {
              font-size: 0.9rem;
            }
      }

      @media ${device.mobileSmall} {
          h1 {
                font-size: 1.8rem;
            }

            h2 {
                font-size: 1.6rem;
            }

            h3 {
                font-size: 1.4rem;
            }

            h4 {
                font-size: 1.2rem;
            }

            h5 {
                font-size: 1.1rem;
            }

            h6 {
                font-size: 1rem;
            }
      }

        p {
            margin: 0;
        }

        table {
            border-collapse: collapse;
        }

        small {
            font-size: 0.7em;
        }
`;

export default globalStyle;
