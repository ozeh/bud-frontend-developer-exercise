import React from 'react';
import Loader from 'react-loader-spinner';
import { colors } from '../../shared/colors';

const LoaderComponent = () => {
  return (
    <Loader
      type="Puff"
      color={colors.success}
      height={150}
      width={150}
      timeout={3000}
    />
  );
};

export default LoaderComponent;
