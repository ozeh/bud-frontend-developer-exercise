import styled from 'styled-components';
import { colors } from '../../../shared/colors';

const Wrapper = styled.div`
  display: flex;
  margin-bottom: auto;
  padding: 1rem;
  justify-content: center;
  justify-items: center;
  flex-wrap: wrap;
  text-align: center;
`;

const AccountType = styled.h5`
  width: 100%;
  flex: 1;
  margin-bottom: 1rem;
`;

const MainAmount = styled.h2`
  text-decoration: underline;
  text-decoration-color: ${colors.warning};
  width: 100%;
  margin-bottom: 1rem;
`;
const LabelValuePair = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding: 0.5rem;
`;

const Label = styled.h6`
  margin: 0 0.2rem;
  color: ${colors.grey};
`;

const Value = styled.h6`
  margin: 0 0.2rem;
`;

const Row = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  justify-items: center;
  align-items: center;
`;

export default {
  Wrapper,
  AccountType,
  MainAmount,
  LabelValuePair,
  Label,
  Value,
  Row,
};
