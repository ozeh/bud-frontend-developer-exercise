import React from 'react';
import { ProviderTypes, BalanceTypes } from '../../../shared/types/account';
import { formatCurrency } from '../../../shared/utils';
import Card from '../Card/Card';
import Styled from './BasicInfoArea.styles';

interface Props {
  balance: BalanceTypes;
  provider: ProviderTypes;
}

const BasicInforArea: React.FC<Props> = ({ balance, provider }) => {
  const { account_number, sort_code, description } = provider;
  const { amount, currency_iso } = balance;

  const formattedAmount = formatCurrency(amount, currency_iso);

  return (
    <Styled.Wrapper>
      <Styled.AccountType>{description || ''}</Styled.AccountType>
      <Styled.MainAmount>{formattedAmount || ''}</Styled.MainAmount>
      <Styled.Row>
        <Card />
      </Styled.Row>
      <Styled.Row>
        <Styled.LabelValuePair>
          <Styled.Label>Sort code:</Styled.Label>
          <Styled.Value>{sort_code}</Styled.Value>
        </Styled.LabelValuePair>
        <Styled.LabelValuePair>
          <Styled.Label>Account number:</Styled.Label>
          <Styled.Value>{account_number}</Styled.Value>
        </Styled.LabelValuePair>
      </Styled.Row>
    </Styled.Wrapper>
  );
};

export default BasicInforArea;
