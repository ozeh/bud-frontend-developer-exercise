import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: auto;
  padding: 1rem;
`;

const LoaderWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: auto;
  justify-items: center;
  justify-content: center;
  align-items: center;
`;

export default {
  Wrapper,
  LoaderWrapper,
};
