import React from 'react';
import { TransactionTypes, BalanceTypes } from '../../../shared/types/account';
import { formatDateToTimestamp } from '../../../shared/utils';
import Transaction from './Transaction';
import Styled from './Transactions.styles';

interface Props {
  transactions: TransactionTypes[];
  currentBalance: BalanceTypes;
}

const sortTransactions = (transactions: TransactionTypes[]) =>
  transactions &&
  transactions.sort(
    (a: TransactionTypes, b: TransactionTypes) =>
      Number(formatDateToTimestamp(b.date)) -
      Number(formatDateToTimestamp(a.date))
  );

const Transactions: React.FC<Props> = ({ transactions, currentBalance }) => {
  let historicalBalance = currentBalance.amount;
  const sortedTransactions = sortTransactions([...transactions]);
  return (
    <Styled.Wrapper>
      <Styled.Heading>Transactions</Styled.Heading>
      {sortedTransactions &&
        sortedTransactions.map((transaction: TransactionTypes, idx: number) => {
          transaction = {
            ...transaction,
            balance: historicalBalance,
          };
          historicalBalance -= transaction.amount.value;
          return <Transaction key={idx} transaction={transaction} />;
        })}
    </Styled.Wrapper>
  );
};

export default Transactions;
