import React from 'react';
import { TransactionTypes } from '../../../shared/types/account';
import { formatCurrency, formatDate } from '../../../shared/utils';
import Styled from './Transactions.styles';

interface Props {
  transaction: TransactionTypes;
}

const Transaction: React.FC<Props> = ({ transaction }) => {
  const { date, description, category_title, amount, balance } = transaction;
  const { value, currency_iso } = amount;

  const formatted = {
    amount: formatCurrency(value, currency_iso),
    date: formatDate(date),
    balance: balance
      ? formatCurrency(balance, currency_iso)
      : formatCurrency(0, currency_iso),
  };

  const isPositiveNumber = value > 0;

  return (
    <Styled.TransactionWrapper>
      <Styled.TransactionHead>
        <Styled.TransactionDate>{formatted.date}</Styled.TransactionDate>
        <Styled.TransactionBalance>
          {formatted.balance}
        </Styled.TransactionBalance>
      </Styled.TransactionHead>
      <Styled.TransactionBody>
        <Styled.TransactionCategory>
          {category_title}
        </Styled.TransactionCategory>
        <Styled.TransactionDescription>
          {description}
        </Styled.TransactionDescription>
        <Styled.TransactionValue isPositiveNumber={isPositiveNumber}>
          {formatted.amount}
        </Styled.TransactionValue>
      </Styled.TransactionBody>
    </Styled.TransactionWrapper>
  );
};

export default Transaction;
