import styled, { css } from 'styled-components';
import { colors } from '../../../shared/colors';

const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-items: center;
  justify-content: center;
  padding: 1rem;
  flex-direction: column;
  width: 100%;
`;

const Heading = styled.h5`
  width: 100%;
  flex: 1;
  margin-bottom: 1rem;
  text-align: center;
`;

const TransactionWrapper = styled.div`
  border-radius: 0.5rem;
  margin-bottom: 0.5rem;
  background-color: ${colors.lightGrey};
  padding: 1rem;
`;

const TransactionHead = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  margin-bottom: 0.5rem;
`;

const TransactionDate = styled.small`
  font-weight: bold;
`;

const TransactionBalance = styled.small``;

const TransactionBody = styled.div`
  width: 100%;
  display: flex;
  align-items: baseline;
  flex-wrap: wrap;
`;

const TransactionCategory = styled.span`
  padding: 0.25rem;
  background-color: ${colors.grey};
  color: ${colors.white};
  border-radius: 0.25rem;
  margin-right: 1rem;
`;

const TransactionDescription = styled.span`
  padding: 0.25rem;
  color: ${colors.grey};
`;

const TransactionValue = styled.h5<{ isPositiveNumber: boolean }>`
  color: ${colors.danger};
  margin-left: auto;
  ${(props) =>
    props.isPositiveNumber &&
    css`
      color: ${colors.success};
    `}
`;

export default {
  Wrapper,
  TransactionWrapper,
  Heading,
  TransactionHead,
  TransactionBody,
  TransactionDate,
  TransactionBalance,
  TransactionCategory,
  TransactionDescription,
  TransactionValue,
};
