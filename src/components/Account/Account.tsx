import React, { useState, useEffect } from 'react';
import api from '../../shared/api/api';
import { AccountInfoTypes } from '../../shared/types/account';
import Loader from '../loader/Loader';
import Styled from './Account.styles';
import BasicInfoArea from './BasicInfoArea/BasicInfoArea';
import Transactions from './Transactions/Transactions';

const Account = () => {
  const [accountInfo, setAccountInfo] = useState<AccountInfoTypes | null>(null);

  useEffect(() => {
    api
      .loadAccountInfo()
      .then((res: any) => setAccountInfo(res.data))
      .catch((err) => console.log(err));
  }, []);

  return accountInfo ? (
    <Styled.Wrapper>
      <BasicInfoArea
        provider={accountInfo.provider}
        balance={accountInfo.balance}
      />
      <Transactions
        transactions={accountInfo.transactions}
        currentBalance={accountInfo.balance || 0}
      />
    </Styled.Wrapper>
  ) : (
    <Styled.LoaderWrapper>
      <Loader />
    </Styled.LoaderWrapper>
  );
};

export default Account;
