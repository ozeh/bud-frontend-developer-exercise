import React from 'react';
import globalStyle from './shared/globalStyle';
import Account from './components/Account/Account';
import Styled from './App.styles';

const App = () => {
  const AppStyle = React.memo(globalStyle);
  return (
    <Styled.Wrapper>
      <AppStyle />
      <Account />
    </Styled.Wrapper>
  );
};

export default App;
