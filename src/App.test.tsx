import React from 'react';
import ReactDOM from 'react-dom';
import { act } from '@testing-library/react';
import App from './App';

describe('App.tsx', () => {
  const div = document.createElement('div');

  it('Renders without crashing', () => {
    act(() => {
      ReactDOM.render(<App />, div);
    });

    setTimeout(() => ReactDOM.unmountComponentAtNode(div));
  });
});
